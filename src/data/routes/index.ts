import route1 from './route1.json'
import route2 from './route2.json'
import route3 from './route3.json'
import route4 from './route4.json'
import route5 from './route5.json'
import route6 from './route6.json'
import route7 from './route7.json'
import route8 from './route8.json'
import route9 from './route9.json'
import route10 from './route10.json'
import route11 from './route11.json'
import route12 from './route12.json'
import route13 from './route13.json'
import route14 from './route14.json'
import route15 from './route15.json'
import route16 from './route16.json'
import route17 from './route17.json'
import route18 from './route18.json'
import route19 from './route19.json'
import route20 from './route20.json'
import route21 from './route21.json'
import route22 from './route22.json'
import route23 from './route23.json'
import route24 from './route24.json'
import route25 from './route25.json'
import route26 from './route26.json'
import route27 from './route27.json'
import route28 from './route28.json'
import route29 from './route29.json'
import route30 from './route30.json'
import route31 from './route31.json'
import route32 from './route32.json'
import route33 from './route33.json'
import route34 from './route34.json'
import route35 from './route35.json'
import route36 from './route36.json'
import route37 from './route37.json'
import route38 from './route38.json'
import route39 from './route39.json'
import route40 from './route40.json'
import route41 from './route41.json'
import route42 from './route42.json'
import route43 from './route43.json'
import route44 from './route44.json'
import route45 from './route45.json'
import route46 from './route46.json'
import route47 from './route47.json'
import route48 from './route48.json'
import route49 from './route49.json'
import route50 from './route50.json'
import route51 from './route51.json'
import route52 from './route52.json'
import route53 from './route53.json'
import route54 from './route54.json'
import route55 from './route55.json'
import route56 from './route56.json'
import route57 from './route57.json'
import route58 from './route58.json'
import route59 from './route59.json'
import route60 from './route60.json'
import route61 from './route61.json'
import route62 from './route62.json'
import route63 from './route63.json'
import route64 from './route64.json'
import route65 from './route65.json'
import route66 from './route66.json'
import route67 from './route67.json'
import allStopsGeoJSON from './stopsGeo.json';

export type AllRoutes = {
  [key: string]: any
}

const allRoutes: AllRoutes = {
  '1': route1,
  '2': route2,
  '3': route3,
  '4': route4,
  '5': route5,
  '6': route6,
  '7': route7,
  '8': route8,
  '9': route9,
  '10': route10,
  '11': route11,
  '12': route12,
  '13': route13,
  '14': route14,
  '15': route15,
  '16': route16,
  '17': route17,
  '18': route18,
  '19': route19,
  '20': route20,
  '21': route21,
  '22': route22,
  '23': route23,
  '24': route24,
  '25': route25,
  '26': route26,
  '27': route27,
  '28': route28,
  '29': route29,
  '30': route30,
  '31': route31,
  '32': route32,
  '33': route33,
  '34': route34,
  '35': route35,
  '36': route36,
  '37': route37,
  '38': route38,
  '39': route39,
  '40': route40,
  '41': route41,
  '42': route42,
  '43': route43,
  '44': route44,
  '45': route45,
  '46': route46,
  '47': route47,
  '48': route48,
  '49': route49,
  '50': route50,
  '51': route51,
  '52': route52,
  '53': route53,
  '54': route54,
  '55': route55,
  '56': route56,
  '57': route57,
  '58': route58,
  '59': route59,
  '60': route60,
  '61': route61,
  '62': route62,
  '63': route63,
  '64': route64,
  '65': route65,
  '66': route66,
  '67': route67,
}

export default allRoutes

export { allStopsGeoJSON }