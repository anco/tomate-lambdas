import { APIGatewayProxyHandler } from 'aws-lambda';
import allSchedules from '../data/schedules';


export const handler: APIGatewayProxyHandler = async (event, context) => {

  const host = event.headers.origin
  let allowOrigin = 'https://colectivos.perpendicul.ar'
  if (host && host === 'http://local.perpendicul.ar:8000') {
    allowOrigin = host
  }

  const routeId = event.pathParameters?.routeId || ''
  const code = event.pathParameters?.code || ''
  const direction = event.pathParameters?.direction || ''

  if (routeId && code && direction) {
    const schedule = allSchedules[`${routeId}_${code}_${direction}_98`]

    if (schedule) {
      return {
        statusCode: 200,
        headers: {
          "Content-Type": "text/json",
          "Access-Control-Allow-Headers": "Content-Type",
          "Access-Control-Allow-Origin": allowOrigin,
          "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
        },
        body: JSON.stringify(schedule),
      };
    }
  }

  return {
    statusCode: 404,
    headers: {
      "Content-Type": "text/json",
      "Access-Control-Allow-Headers": "Content-Type",
      "Access-Control-Allow-Origin": allowOrigin,
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
    },
    body: '{}',
  };
}