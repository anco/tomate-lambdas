import { APIGatewayProxyHandler } from 'aws-lambda';
// Nasty
import allRoutes from '../data/routes/';

export const handler: APIGatewayProxyHandler = async (event, context) => {

  const host = event.headers.origin
  let allowOrigin = 'https://colectivos.perpendicul.ar'
  if (host && host === 'http://local.perpendicul.ar:8000') {
    allowOrigin = host
  }

  const id = event.pathParameters?.id || ''
  const route = allRoutes[id]

  if (route) {
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/json",
        "Access-Control-Allow-Headers": "Content-Type",
        "Access-Control-Allow-Origin": allowOrigin,
        "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
      },
      body: JSON.stringify(route)
    }
  }

  return {
    statusCode: 404,
    headers: {
      "Content-Type": "text/json",
      "Access-Control-Allow-Headers": "Content-Type",
      "Access-Control-Allow-Origin": allowOrigin,
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
    },
    body: '{}',
  };
}