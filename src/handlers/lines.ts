import { APIGatewayProxyHandler } from 'aws-lambda';
import data from './linesAndRoutes.json';


export const handler: APIGatewayProxyHandler = async (event, context) => {

  const host = event.headers.origin
  let allowOrigin = 'https://colectivos.perpendicul.ar'
  if (host && host === 'http://local.perpendicul.ar:8000') {
    allowOrigin = host
  }

  return {
    statusCode: 200,
    headers: {
      "Content-Type": "text/json",
      "Access-Control-Allow-Headers": "Content-Type",
      "Access-Control-Allow-Origin": allowOrigin,
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
    },
    body: JSON.stringify(data),
  };
}