import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as apigw from 'aws-cdk-lib/aws-apigateway';
import * as lambda from 'aws-cdk-lib/aws-lambda-nodejs';
import * as path from 'path';
import { Runtime } from 'aws-cdk-lib/aws-lambda';
import * as logs from 'aws-cdk-lib/aws-logs';

export class TomateLambdasStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const api = new apigw.RestApi(this, `TomateApiGateway`, {
      restApiName: `tomate-api`,
      deployOptions: {
        metricsEnabled: true,
        loggingLevel: apigw.MethodLoggingLevel.INFO,
        dataTraceEnabled: true,
      },
      cloudWatchRole: true,
    });

    const getLines = new lambda.NodejsFunction(this, `GetLinesFunction`, {
      entry: path.resolve(__dirname, '../src/handlers/lines.ts'),
      functionName: `tomate-api-get-lines-and-routes`,
      handler: 'handler',
      memorySize: 128,
      environment: {},
      runtime: Runtime.NODEJS_LATEST,
      timeout: cdk.Duration.seconds(15),
      bundling: {
        target: 'es2020',
      },
      logRetention: logs.RetentionDays.ONE_MONTH,
    });


    const getRoute = new lambda.NodejsFunction(this, `GetRouteFunction`, {
      entry: path.resolve(__dirname, '../src/handlers/routes.ts'),
      functionName: `tomate-api-get-route`,
      handler: 'handler',
      memorySize: 128,
      environment: {},
      runtime: Runtime.NODEJS_LATEST,
      timeout: cdk.Duration.seconds(15),
      bundling: {
        target: 'es2020',
      },
      logRetention: logs.RetentionDays.ONE_MONTH,
    });

    const getSchedule = new lambda.NodejsFunction(this, `GetScheduleFunction`, {
      entry: path.resolve(__dirname, '../src/handlers/schedules.ts'),
      functionName: `tomate-api-get-schedule`,
      handler: 'handler',
      memorySize: 128,
      environment: {},
      runtime: Runtime.NODEJS_LATEST,
      timeout: cdk.Duration.seconds(15),
      bundling: {
        target: 'es2020',
      },
      logRetention: logs.RetentionDays.ONE_MONTH,
    });

    const getStopsGeoJSON = new lambda.NodejsFunction(this, `GetStopsGeoJSONFunction`, {
      entry: path.resolve(__dirname, '../src/handlers/stopsGeoJSON.ts'),
      functionName: `tomate-api-get-stops-geo-json`,
      handler: 'handler',
      memorySize: 128,
      environment: {},
      runtime: Runtime.NODEJS_LATEST,
      timeout: cdk.Duration.seconds(15),
      bundling: {
        target: 'es2020',
      },
      logRetention: logs.RetentionDays.ONE_MONTH,
    });

    const getLinesLambdaIntegration = new apigw.LambdaIntegration(getLines);
    const getRouteLambdaIntegration = new apigw.LambdaIntegration(getRoute);
    const getScheduleLambdaIntegration = new apigw.LambdaIntegration(getSchedule);
    const getStopsGeoJSONLambdaIntegration = new apigw.LambdaIntegration(getStopsGeoJSON);

    const linesResource = api.root.addResource("lines");
    linesResource.addMethod("GET", getLinesLambdaIntegration);

    const routeResource = api.root.addResource("routes");
    const routeIdResource = routeResource.addResource("{id}")
    routeIdResource.addMethod("GET", getRouteLambdaIntegration);

    const scheduleResource = api.root.addResource("schedules");
    const scheduleRouteIdResource = scheduleResource.addResource("{routeId}")
    const scheduleCodeResource = scheduleRouteIdResource.addResource("{code}")
    const scheduleDirectionResource = scheduleCodeResource.addResource("{direction}")
    scheduleDirectionResource.addMethod("GET", getScheduleLambdaIntegration);

    const stopsResource = api.root.addResource("stops");
    stopsResource.addMethod("GET", getStopsGeoJSONLambdaIntegration);

  }
}
